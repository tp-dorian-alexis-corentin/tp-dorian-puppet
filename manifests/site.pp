#déclarer un node. Modules: Pare-feu (UFW), Apache, MySQL ou MariaDB (au choix), PHP...
#Pare-feu UFW
include ufw
node default { 
  
  class { 'ufw':
  forward => 'ACCEPT',
  }

  ufw::allow { 'ssh':
  port => '22'
  }

  ufw::allow { 'http':
  port => '80'
  }

  #Apache
  class { 'apache': }

   file { '/data/www':
    ensure => 'directory',
    mode   => '0777',
  }

  apache::vhost {'silly-hugle.dgr.ovh':
    port => '80',
    docroot => '/data/www',
    docroot_owner => 'www-data',
    docroot_group => 'www-data',
  }


  #MySQL
  class { '::mysql::server':
  root_password           => 'alexiscorentindorian',
  remove_default_accounts => true,
  restart                 => true,
  host     => 'localhost',
  }

  #PHP
  class { '::php':
  ensure       => latest,
  manage_repos => true,
  fpm          => true,
  dev          => true,
  composer     => false,
  pear         => false,
  phpunit      => false,
  }
}

 #commit, pull, puppet agent -t, voir si ça plante ou pas.
